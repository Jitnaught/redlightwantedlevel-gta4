This is a script I wrote as an example for a friend I had at the time. He wanted to know how he could make a mod where you would get a wanted level if you went through a red stop light.

He ended up taking the code, tweaking it, compiling it, and then releasing it under his name, crediting me only for "helping."

Anyway...

There are no controls in-game. 

There are some INI settings to change if the script should be enabled or not, how many stars to give you if cops see you go through a red light, and how far the cops can see you.

Installation:
Install an ASI Loader (such as YAASIL).
Install .NET ScriptHook.
Copy the .dll and .ini files to the scripts folder within your GTA IV directory.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
