﻿using GTA;
using System;
using System.Windows.Forms;

namespace RedLightWantedLevel
{
    public class RedLightWantedLevel : Script
    {
        private bool enabled = true;
        private float distanceCops = 37.5f;

        public RedLightWantedLevel()
        {
            Interval = 50;
            Tick += RedLightWantedLevel_Tick;
        }

        private void RedLightWantedLevel_Tick(object sender, EventArgs e)
        {
            int timeLastRanLight = GET_TIME_SINCE_PLAYER_RAN_LIGHT(Player);
            Game.Console.Print("Last: " + timeLastRanLight.ToString());
            if (enabled && Player.WantedLevel == 0 && Player.Character.isInVehicle() && timeLastRanLight > -1 && timeLastRanLight < 2000)
            {
                Ped[] closePeds = World.GetPeds(Player.Character.Position, distanceCops);
                foreach (Ped ped in closePeds)
                {
                    if (ped.Model == Model.BasicCopModel || ped.Model == Model.CurrentCopModel && Math.Abs(Player.Character.Position.Z - ped.Position.Z) <= 20)
                    {
                        Player.WantedLevel = 1;
                        break;
                    }
                }
            }
        }

        private int GET_TIME_SINCE_PLAYER_RAN_LIGHT(Player player)
        {
            return GTA.Native.Function.Call<int>("GET_TIME_SINCE_PLAYER_RAN_LIGHT", player);
        }
    }
}
